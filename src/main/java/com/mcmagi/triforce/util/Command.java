package com.mcmagi.triforce.util;

import java.io.Serializable;

/**
 * Used to lazily load a value into a cache.
 * @author Michael C. Maggio
 */
public interface Command<T> extends Serializable
{
    /**
     * Executes the command.
     * @param params Parameters
     * @return Return value
     * @throws Exception A possible exception thrown by the command.
     */
    public T execute(Object ... params) throws Exception;
}
