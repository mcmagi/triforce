package com.mcmagi.triforce.util;

/**
 * An interface for components that may be "started".
 * @author Michael C. Maggio
 */
public interface Startable
{
    /**
     * Starts the component.
     */
    public void start();
}
