package com.mcmagi.triforce.util;

/**
 * An interface for components that may be "closed".
 * @author Michael C. Maggio
 */
public interface Closeable
{
    /**
     * Closes the component.
     */
    public void close();
}
