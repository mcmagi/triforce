package com.mcmagi.triforce.link;

import java.io.Serializable;

/**
 * Represents a link to another object.  For reference, the 'linking object'
 * is where the Link is contained or defined, and the 'linked object' is the
 * object contained within or abstracted by the Link.  Links are used to hide
 * the implementation of how the object is connected to the linking object.
 * Links are expected to be serializable.
 * @author Michael C. Maggio
 */
public interface Link<T> extends Serializable
{
    /**
     * Returns the linked object.
     * @return Linked object
     * @throws LinkException (runtime)
     */
    public T get(Object ... params)
        throws LinkException;

    /**
     * Replaces the linked object with the specified value.
     * @param value New linked object
     * @throws LinkException (runtime)
     */
    public void set(T value)
        throws LinkException;
}
