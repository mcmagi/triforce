package com.mcmagi.triforce.link;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A map where values are links that are resolved on an as-needed basis.  The
 * class requires a link to a set of keys provided on construction.  The key set
 * is allowed to vary via the link, however the LinkMap will not update the
 * linked key set if values are added, therefore any modification operation that
 * requires changing the key set is not allowed.
 * 
 * On construction, a LinkFactory is also required to create link objects as
 * needed.  The LinkFactory.createLink() method will be passed the key associated
 * with the link.
 * 
 * @author Michael C. Maggio
 */
public class LinkMap<K,V> extends AbstractMap<K,V> implements Serializable
{
    private Map<K,Link<V>> map = new HashMap<>();
    private Link<Set<K>> keys;
    private LinkFactory<K,V> factory;

    /**
     * Creates a LinkMap.
     * @param keys Link to set of supported keys
     * @param factory LinkFactory for creation of links by key
     */
    public LinkMap(Link<Set<K>> keys, LinkFactory<K,V> factory)
    {
        this.keys = keys;
        this.factory = factory;
    }

    /**
     * Returns a dynamic set of entries in the LinkMap.
     * @return Entry set
     */
    @Override
    public Set<Entry<K, V>> entrySet()
    {
        return new LinkEntrySet();
    }

    /**
     * Resolves and replaces the link value for the given key.
     * @param key Key
     * @param value Value
     * @return Value
     */
    @Override
    public V put(K key, V value)
    {
        getLink(key).set(value);
        return value;
    }

    /**
     * Returns true if the key set link contains the specified key.
     * @param key
     * @return True if contained
     */
    @Override
    public boolean containsKey(Object key)
    {
        return keys.get().contains((K) key);
    }

    /**
     * Resolves and returns the link value for the given key.
     * @param key Key
     * @return Value
     */
    @Override
    public V get(Object key)
    {
        return getLink((K) key).get();
    }

    /**
     * Not supported.
     * @param key Key
     * @return Value
     */
    @Override
    public V remove(Object key)
    {
        throw new UnsupportedOperationException("Removal not supported.");
    }

    /**
     * Returns the link object for the given key.  If the key is not specified in
     * the key set passed on construction, then an IllegalArgumentException is thrown.
     * @param key Key
     * @return Link to value
     */
    private Link<V> getLink(K key)
    {
        if (! map.containsKey(key))
        {
            // check if this key is allowed
            if (! keys.get().contains(key))
                throw new IllegalArgumentException("specified key '" + key + "' not in list of valid keys for LinkMap");

            // create the link to the value for this key
            map.put(key, factory.createLink(key));
        }

        return map.get(key);
    }

    /**
     * Represents an entry in the LinkMap.  Entry value links are resolved on
     * calls to getValue() and setValue().
     */
    private class LinkEntry implements Entry<K,V>
    {
        private K key;

        public LinkEntry(K key)
        {
            this.key = key;
        }

        @Override
        public K getKey()
        {
            return key;
        }

        @Override
        public V getValue()
        {
            return getLink(key).get();
        }

        @Override
        public V setValue(V value)
        {
            getLink(key).set(value);
            return value;
        }
    }

    /**
     * A dynamic set that represents the set of entries in the link map.  The
     * values are based on the set of keys provided on construction.
     */
    private class LinkEntrySet extends AbstractSet<Entry<K,V>>
    {
        /**
         * Returns an iterator over the link entries.
         * @return 
         */
        @Override
        public Iterator<Entry<K,V>> iterator()
        {
            return new LinkEntryIterator();
        }

        @Override
        public int size()
        {
            return keys.get().size();
        }

        @Override
        public boolean contains(Object o)
        {
            Entry<K,V> entry = (Entry<K,V>) o;
            return keys.get().contains(entry.getKey());
        }

        @Override
        public boolean removeAll(Collection<?> c)
        {
            throw new UnsupportedOperationException("Removal not supported.");
        }

        @Override
        public boolean remove(Object o)
        {
            throw new UnsupportedOperationException("Removal not supported.");
        }
    }

    /**
     * Iterates over all keys, creating LinkEntry objects on each call to next().
     */
    private class LinkEntryIterator implements Iterator<Entry<K,V>>
    {
        private Iterator<K> keyIterator = keys.get().iterator();

        /**
         * Returns true if there are more entries.
         * @return True if more entries
         */
        @Override
        public boolean hasNext()
        {
            return keyIterator.hasNext();
        }

        /**
         * Returns the next entry in the map.
         * @return Next entry
         */
        @Override
        public Entry<K,V> next()
        {
            return new LinkEntry(keyIterator.next());
        }

        /**
         * Not supported.
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException("Removal not supported.");
        }
    }
}
