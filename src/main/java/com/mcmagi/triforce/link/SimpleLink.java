package com.mcmagi.triforce.link;

/**
 * A link that contains a Java object.  De-referncing the link will simply
 * return the contained object.
 * @author Michael C. Maggio
 */
public class SimpleLink<T> implements Link<T>
{
    private T value;

    /**
     * Creates a simple link with no initial value.
     */
    public SimpleLink()
    {
    }

    /**
     * Creates a link with an initial value.
     * @param value 
     */
    public SimpleLink(T value)
    {
        this.value = value;
    }

    @Override
    public T get(Object ... params)
    {
        return value;
    }

    @Override
    public void set(T value)
    {
        value = value;
    }
}
