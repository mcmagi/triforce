package com.mcmagi.triforce.link;

import com.mcmagi.triforce.cache.Cache;
import com.mcmagi.triforce.cache.CacheException;


/**
 * A link that contains a cache to a Java object.  De-referencing the link will
 * ask the cache to return the cached Java object.  This may result in the cache
 * reloading its contents as necessary.
 * @author Michael C. Maggio
 */
public class CacheLink<T> implements Link<T>
{
    private Cache<T> cache;

    public CacheLink(Cache<T> cache)
    {
        this.cache = cache;
    }

    @Override
    public T get(Object ... params)
    {
        try
        {
            return cache.get();
        }
        catch (CacheException ex)
        {
            throw new LinkException("could not load link", ex);
        }
    }

    @Override
    public void set(T value)
    {
        cache.set(value);
    }

    public void expire()
    {
        cache.expire();
    }
}
