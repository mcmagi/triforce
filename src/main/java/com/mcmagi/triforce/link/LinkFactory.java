package com.mcmagi.triforce.link;

import com.mcmagi.triforce.util.Command;
import com.mcmagi.triforce.cache.Cache;
import java.io.Serializable;

/**
 * A factory class for various types of Link implementations.
 * @author Michael C. Maggio
 */
public abstract class LinkFactory<K,V> implements Serializable
{
    /**
     * Returns a custom Link implementation.
     * @param obj
     * @return 
     */
    public abstract Link<V> createLink(K key);

    /**
     * Creates a link around a Java object.  De-referencing the link will
     * simply return the Java object.
     * @param command
     * @return Simple link
     */
    public static <T> Link<T> createSimpleLink(T obj)
    {
        return new SimpleLink<>(obj);
    }

    /**
     * Creates a link around a cached object.  De-referencing the link will 
     * return the value in the cache, refreshing the cache if necessary.
     * @param command
     * @return Cache link
     */
    public static <T> Link<T> createCacheLink(Cache<T> cache)
    {
        return new CacheLink<>(cache);
    }

    /**
     * Creates a link around a command object.  De-referencing the link will
     * invoke the command object and return its result.
     * @param command
     * @return Command link
     */
    public static <T> Link<T> createCommandLink(Command<T> command)
    {
        return new CommandLink<>(command);
    }
}
