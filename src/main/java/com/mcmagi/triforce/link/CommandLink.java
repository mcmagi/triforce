package com.mcmagi.triforce.link;

import com.mcmagi.triforce.util.Command;

/**
 * A link to a Command object.  De-referencing the link will result in the
 * Command object immediately being executed with its result value returned.
 * These types of Links cannot be {@link #set}.
 * @author Michael C. Maggio
 */
public class CommandLink<T> implements Link<T>
{
    private Command<T> command;

    public CommandLink(Command<T> cmd)
    {
        this.command = cmd;
    }

    @Override
    public T get(Object ... params)
    {
        try
        {
            return command.execute(params);
        }
        catch (Exception ex)
        {
            throw new LinkException("could not load link", ex);
        }
    }

    @Override
    public void set(T value)
    {
        throw new LinkException("command links are read-only");
    }
}
