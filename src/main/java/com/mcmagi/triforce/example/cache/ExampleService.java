
package com.mcmagi.triforce.example.cache;

import java.util.Map;

/**
 * @author Michael C. Maggio
 */
public interface ExampleService
{
    public SimpleObject getSimpleObject(String id)
		throws ExampleServiceException;

    public Map<String,ComplexObject> getComplexObjects(String ... ids)
        throws ExampleServiceException;
}
