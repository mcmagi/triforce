package com.mcmagi.triforce.example.cache;

/**
 * @author Michael C. Maggio
 */
public class ExampleServiceException extends Exception
{
	public ExampleServiceException(String message)
	{
		super(message);
	}

	public ExampleServiceException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
