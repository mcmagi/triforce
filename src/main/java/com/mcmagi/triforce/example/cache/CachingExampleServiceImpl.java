package com.mcmagi.triforce.example.cache;

import com.mcmagi.triforce.cache.Cache;
import com.mcmagi.triforce.cache.CacheException;
import com.mcmagi.triforce.cache.CacheFactory;
import com.mcmagi.triforce.cache.CacheMap;
import com.mcmagi.triforce.util.Command;
import com.mcmagi.triforce.cache.MultiValueQueryCacheMap;
import com.mcmagi.triforce.cache.manager.CacheManager;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Caches the application-level data returned from the example service.
 * @author Michael C. Maggio
 */
public class CachingExampleServiceImpl implements ExampleService
{
    private static final Log LOG = LogFactory.getLog(CachingExampleServiceImpl.class);

    public static final String SIMPLE_OBJECT_CACHE_NAME = "example.simpleObject";
    public static final String COMPLEX_OBJECT_CACHE_NAME = "example.complexObject";

    private CacheManager cacheManager;
    private CacheMap<String,SimpleObject> simpleObjectCache;
    private MultiValueQueryCacheMap<String,ComplexObject> complexObjectCache;
	private ExampleService service;

    public void setCacheManager(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

	public void setService(ExampleService service)
	{
		this.service = service;
	}

    public void init()
    {
        simpleObjectCache = cacheManager.createCacheMap(SIMPLE_OBJECT_CACHE_NAME, new CacheFactory<String,SimpleObject>() {
            @Override public Cache<SimpleObject> createCache(final String id) {
				// NOTE: I have no idea what this was intended to mean:
                // using the session cache registry basically avoids having the inner caches managed at an app level;
                // we should devise a better way
                return cacheManager.getSessionCacheRegistry().createLazyCache(
                    SIMPLE_OBJECT_CACHE_NAME, new Command<SimpleObject>() {
                    @Override public SimpleObject execute(Object... params) throws Exception {
                        return service.getSimpleObject(id);
                    }
                });
            }
        });

        complexObjectCache = new MultiValueQueryCacheMap<>(
			// used to perform the query
			new Command<Map<String,ComplexObject>>() {
				@Override public Map<String,ComplexObject> execute(Object ... params) throws Exception {
					Set<String> ids = (Set<String>) params[0];
					return service.getComplexObjects(ids.toArray(new String[0]));
				}
				
			},
			cacheManager.createCacheMap(COMPLEX_OBJECT_CACHE_NAME, new CacheFactory<String,ComplexObject>() {
            	@Override public Cache<ComplexObject> createCache(final String id) {
                	// using the session cache registry basically avoids having the inner caches managed at an app level;
                	// we should devise a better way
                	return cacheManager.getSessionCacheRegistry().createSimpleCache(COMPLEX_OBJECT_CACHE_NAME, ComplexObject.class);
            	}
        }));
    }

    @Override
    public SimpleObject getSimpleObject(String id)
        throws ExampleServiceException
    {
        try
        {
            return simpleObjectCache.get(id);
        }
        catch (CacheException ex)
        {
            throw new ExampleServiceException("failed to load simple object cache for id=" + id, ex);
        }
    }

    @Override
    public Map<String,ComplexObject> getComplexObjects(String ... ids)
        throws ExampleServiceException
    {
        Set<String> idSet = new HashSet<>(Arrays.asList(ids));

        try
        {
            return complexObjectCache.get(idSet);
        }
        catch (CacheException ex)
        {
            throw new ExampleServiceException("failed to load complex object cache for ids=" + new TreeSet<>(idSet), ex);
        }
    }
}
