package com.mcmagi.triforce.cache.manager;

import com.mcmagi.triforce.cache.Cache;
import com.mcmagi.triforce.util.Command;

/**
 * @author Michael C. Maggio
 */
public interface CacheRegistry
{
    public <T> Cache<T> createSimpleCache(String key, Class<T> clazz);

    public <T> Cache<T> createLazyCache(String key, Command<T> command);

    public <T> Cache<T> createActiveCache(String key, Command<T> command);
}
