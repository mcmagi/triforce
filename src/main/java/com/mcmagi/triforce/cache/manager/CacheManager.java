package com.mcmagi.triforce.cache.manager;

import com.mcmagi.triforce.cache.ActiveCache;
import com.mcmagi.triforce.cache.Cache;
import com.mcmagi.triforce.cache.CacheFactory;
import com.mcmagi.triforce.cache.CacheMap;
import com.mcmagi.triforce.util.Closeable;
import com.mcmagi.triforce.util.Command;
import com.mcmagi.triforce.cache.LazyCache;
import com.mcmagi.triforce.cache.ManagedCache;
import com.mcmagi.triforce.cache.SimpleCache;
import com.mcmagi.triforce.util.Startable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.TaskScheduler;


/**
 * A factory class for cached attributes.  Must be configured with a
 * TaskScheduler to handle the construction of active caches.
 * @author Michael C. Maggio
 */
public class CacheManager implements CacheRegistry
{
    private static final Log LOG = LogFactory.getLog(CacheManager.class);

	private TaskScheduler scheduler;
	private Map<String,ManagedCache> cacheMap = new ConcurrentHashMap<>();
	private Map<String,CacheConfig> configMap = new ConcurrentHashMap<>();
    private SessionCacheRegistry sessionCacheRegistry = new SessionCacheRegistry(this);

    public void setScheduler(TaskScheduler scheduler)
    {
        this.scheduler = scheduler;
    }

    public TaskScheduler getScheduler()
    {
        return scheduler;
    }

    public CacheRegistry getSessionCacheRegistry()
    {
        return sessionCacheRegistry;
    }

    public CacheConfig getConfig(String key)
    {
        return configMap.get(key);
    }

    /**
     * Initializes the CacheManager, starting any thread schedules needed for
     * active caches.  Since the active caches may depend on other application
     * resources, the call to this method should be delayed until all
     * dependencies have been satisfied.  In spring, the recommended way to
     * do this is after a ContextRefreshedEvent.
     * @see CacheStartupListener
     */
    public void start()
    {
        LOG.debug("starting all managed caches");
        for (ManagedCache cache : cacheMap.values())
            startCache(cache);
    }

    /**
     * Destroys the CacheManager, cleaning up after any resources and threads.
     * This should be called on application shutdown.
     */
    public void destroy()
    {
        LOG.debug("destroying all managed caches");
        for (ManagedCache cache : cacheMap.values())
            destroyCache(cache);
    }

	public Set<String> cachedItems()
    {
		return cacheMap.keySet();
	}

	public String getDisplayName(String cachedItem) {
		if (configMap.containsKey(cachedItem))
			return configMap.get(cachedItem).getDisplayName();
        else if (hasCache(cachedItem))
			return cachedItem;
        return null;
	}

    /**
     * Returns true if there is a cache configured with the specified key.
     * @param cachedItem
     * @return 
     */
    public boolean hasCache(String cachedItem) {
        return cacheMap.containsKey(cachedItem);
    }

	public void expire(String cachedItem) {
		if(hasCache(cachedItem))
			cacheMap.get(cachedItem).expire();
	}

    /**
     * Returns an actively loading instance of a cache.  Active caches
     * created by the cache manager are properly cleaned up upon the call to
     * CacheManager.close().
     * @param <T> The class to be contained by the cached attribute
	 * @param key Unique name for data being cached
	 * @param command Value loader
     * @return Cached attribute
     */
    @Override
    public <T> Cache<T> createActiveCache(String key, Command<T> command)
    {
        return createCache(key, createActiveCacheFactory(command));
    }

    /**
     * Returns a simple instance of a cached attribute.
     * @param <T> The class to be contained by the cached attribute
	 * @param key Unique name for data being cached
     * @param clazz The generic class instance
     * @return Cached attribute
     */
    @Override
    public <T> Cache<T> createSimpleCache(String key, Class<T> clazz)
    {
        return createCache(key, createSimpleCacheFactory(clazz));
    }

    /**
     * Returns an actively loading instance of a cached attribute.
     * @param <T> The class to be contained by the cached attribute
	 * @param key Unique name for data being cached
     * @param command Value loader
     * @return Cached attribute
     */
    @Override
    public <T> Cache<T> createLazyCache(String key, Command<T> command)
    {
        return createCache(key, createLazyCacheFactory(command));
    }

    /**
     * Returns an instance of a cached attribute to be managed by the CacheManager.
     * @param <T> The class to be contained by the cached attribute
     * @param key Unique name for data being cached
     * @param factory Factory to create the cache
     * @return Cached attribute
     */
    public <T> Cache<T> createCache(String key, CacheFactory<CacheConfig,T> factory)
    {
        return createCache(key, factory, true);
    }

    /**
     * Returns an instance of a cached attribute to be managed by the CacheManager.
     * @param <T> The class to be contained by the cached attribute
     * @param key Unique name for data being cached
     * @param factory Factory to create the cache
     * @param managed Whether to cache is to be managed
     * @return Cached attribute
     */
    public <T> Cache<T> createCache(String key, CacheFactory<CacheConfig,T> factory, boolean managed)
    {
        // get config
		CacheConfig conf = configMap.get(key);
        if (conf == null)
			LOG.warn("Cache config not set for " + key);

        // create and manage cache
        Cache<T> cache = factory.createCache(conf);
        if (managed)
		    cacheMap.put(key, cache);
        return cache;
    }

    public <K,V> CacheMap<K,V> createCacheMap(String key, CacheFactory<K,V> factory)
    {
        return createCacheMap(key, factory, true);
    }

    public <K,V> CacheMap<K,V> createCacheMap(String key, CacheFactory<K,V> factory, boolean managed)
    {
        // get config
		//CacheConfig conf = configMap.get(key);
        //int expireSeconds = 0;
        //if (conf == null)
			//LOG.warn("Cache config not set for " + key);
        //else
            //expireSeconds = conf.getExpireSeconds();

        // create and manage cache
        CacheMap<K,V> cache = new CacheMap<>(factory);
        if (managed)
		    cacheMap.put(key, cache);
        return cache;
    }

    /**
     * Initializes any resources on the specified cached attribute.  After this method
     * is called, there is no guarantee that it will be usable again.  Intended for
     * actively cached attributes to begin scheduling threads.
     * @param cache Cached attribute
     */
    public void startCache(ManagedCache cache)
    {
        if (cache instanceof Startable)
            ((Startable) cache).start();
    }

    /**
     * Cleans up any resources on the specified cached attribute.  After this method
     * is called, there is no guarantee that it will be usable again.  Intended for
     * actively cached attributes to clean up any outstanding thread schedules.
     * @param cache Cached attribute
     */
    public void destroyCache(ManagedCache cache)
    {
        if (cache instanceof Closeable)
            ((Closeable) cache).close();

        // SIDE NOTE:
        // It's 'close' patterns like these that make me long for the C++ days.
        // Destructors were incredibly elegant.  Of course, C++ has it's own issues...
    }

    /**
     * Creates a cache factory for active caches.  The returned instance is
     * associated with this cache manager.
     * @param <T> The class to be contained by the cached attribute
     * @param command
     * @return Cache factory
     */
    public <T> CacheFactory<CacheConfig,T> createActiveCacheFactory(Command<T> command)
    {
        return new ActiveCacheFactory<>(command);
    }

    /**
     * Creates a cache factory for simple caches.  The returned instance is
     * associated with this cache manager.
     * @param <T> The class to be contained by the cached attribute
     * @param clazz The generic class instance
     * @return Cache factory
     */
    public <T> CacheFactory<CacheConfig,T> createSimpleCacheFactory(Class<T> clazz)
    {
        return new SimpleCacheFactory<>();
    }

    /**
     * Creates a cache factory for lazy caches.  The returned instance is
     * associated with this cache manager.
     * @param <T> The class to be contained by the cached attribute
     * @param command
     * @return Cache factory
     */
    public <T> CacheFactory<CacheConfig,T> createLazyCacheFactory(Command<T> command)
    {
        return new LazyCacheFactory<>(command);
    }

	/**
	 * @return Map with cache expiration time in seconds
	 */
	public Map<String, CacheConfig> getConfigMap()
	{
		return configMap;
	}

	/**
	 * @param expireMap Map with cache expiration time in seconds
	 */
	public void setConfigMap(Map<String, CacheConfig> configMap)
	{
		this.configMap.clear();
		this.configMap.putAll(configMap);
	}

	public static class CacheConfig
    {
		private String displayName;
		private int expireSeconds = 0;

		public CacheConfig() {
		}

		/**
		 * @return the displayName
		 */
		public String getDisplayName() {
			return displayName;
		}

		/**
		 * @param displayName the displayName to set
		 */
		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		/**
		 * @return the expireSeconds
		 */
		public int getExpireSeconds() {
			return expireSeconds;
		}

		/**
		 * @param expireSeconds the expireSeconds to set
		 */
		public void setExpireSeconds(int expireSeconds) {
			this.expireSeconds = expireSeconds;
		}
	}

    /**
     * A factory class for LazyCache.
     * @param <T> The type to be cached
     */
    public class LazyCacheFactory<T> implements CacheFactory<CacheConfig,T>
    {
        private Command<T> command;

        public LazyCacheFactory(Command<T> command)
        {
            this.command = command;
        }

        @Override
        public Cache<T> createCache(CacheConfig conf)
        {
		    LazyCache<T> lca;
		    if (conf != null) {
                if (LOG.isDebugEnabled())
			        LOG.debug("Cache expiration for "+conf.getDisplayName()+", set to "+conf.getExpireSeconds());
			    lca = new LazyCache<>(command, conf.getExpireSeconds());
		    } else {
			    lca = new LazyCache<>(command);
		    }
            return lca;
        }
    }

    /**
     * A factory class for ActiveCache.
     * @param <T> The type to be cached
     */
    public class ActiveCacheFactory<T> implements CacheFactory<CacheConfig,T>
    {
        private Command<T> command;

        public ActiveCacheFactory(Command<T> loader)
        {
            this.command = loader;
        }

        @Override
        public Cache<T> createCache(CacheConfig conf)
        {
		    ActiveCache<T> aca;
		    if (conf != null) {
                if (LOG.isDebugEnabled())
			        LOG.debug("Cache expiration for "+conf.getDisplayName()+", set to "+conf.getExpireSeconds());
			    aca = new ActiveCache<>(scheduler, command, conf.getExpireSeconds());
		    } else {
			    aca = new ActiveCache<>(scheduler, command);
		    }
            return aca;
        }
    }

    /**
     * A factory class for SimpleCache.
     * @param <T> The type to be cached
     */
    public class SimpleCacheFactory<T> implements CacheFactory<CacheConfig,T>
    {
        @Override
        public Cache<T> createCache(CacheConfig conf)
        {
		    SimpleCache<T> aca;
		    if (conf != null) {
                if (LOG.isDebugEnabled())
			        LOG.debug("Cache expiration for "+conf.getDisplayName()+", set to "+conf.getExpireSeconds());
			    aca = new SimpleCache<>(conf.getExpireSeconds());
		    } else {
			    aca = new SimpleCache<>();
		    }
            return aca;
        }
    }
}
