package com.mcmagi.triforce.cache.manager;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Starts all managed caches when spring is done loading all beans.
 * @author Michael C. Maggio
 */
public class CacheStartupListener implements ApplicationListener<ContextRefreshedEvent>
{
    private CacheManager cacheManager;
    private boolean started = false;

    public void setCacheManager(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    @Override
    public synchronized void onApplicationEvent(ContextRefreshedEvent event)
    {
        if (! started)
        {
            cacheManager.start();
            started = true;
        }
    }
}
