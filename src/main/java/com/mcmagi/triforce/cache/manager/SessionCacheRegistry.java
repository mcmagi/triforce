package com.mcmagi.triforce.cache.manager;

import com.mcmagi.triforce.cache.Cache;
import com.mcmagi.triforce.util.Command;

/**
 * @author Michael C. Maggio
 */
public class SessionCacheRegistry implements CacheRegistry
{
    private CacheManager cacheManager;

    public SessionCacheRegistry(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    @Override
    public <T> Cache<T> createActiveCache(String key, Command<T> command)
    {
        Cache<T> cache = cacheManager.createCache(key, cacheManager.createActiveCacheFactory(command), false);
        return cache;
    }

    @Override
    public <T> Cache<T> createLazyCache(String key, Command<T> command)
    {
        Cache<T> cache = cacheManager.createCache(key, cacheManager.createLazyCacheFactory(command), false);
        return cache;
    }

    @Override
    public <T> Cache<T> createSimpleCache(String key, Class<T> clazz)
    {
        Cache<T> cache = cacheManager.createCache(key, cacheManager.createSimpleCacheFactory(clazz), false);
        return cache;
    }
}
