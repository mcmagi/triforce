package com.mcmagi.triforce.cache;

import com.mcmagi.triforce.util.Command;
import com.mcmagi.triforce.util.Startable;
import com.mcmagi.triforce.util.Closeable;
import java.io.Serializable;
import java.util.Calendar;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.TaskScheduler;

/**
 * An actively refreshing implementation of a Cache.  Upon
 * initialization (via a call to {@link #start()}, a task is scheduled with a
 * {@link TaskSchduler} to fetch the value from the Command.  The task is
 * scheduled to initiate in intervals equal to expirySeconds, with the first
 * execution starting immediately.  If expirySeconds is 0, then the value will
 * only be fetched once and never refreshed.
 *
 * Note that while this class is scheduling threads via a TaskScheduler, some
 * other entity (e.g. Spring) must be responsible for managing the thread pool
 * and destroying it on shutdown.
 * @author Michael C. Maggio
 */
public class ActiveCache<T> implements CachedAttribute<T>, Startable, Closeable
{
    private static final Log LOG = LogFactory.getLog(ActiveCache.class);

    // get task scheduler from spring so that shutdown hooks are propery called
    private TaskScheduler scheduler;

    private int expirySeconds = 0;
    private CacheRefresher refresher;
    private ScheduledFuture<?> future;
    private T value;
    private Command<T> command;
    private boolean initialized = false;

    /**
     * Constructs an active cache using the specified value loader and no expiration.
     * @param scheduler The task scheduler to use for scheduling tasks
     * @param command Value loader
     */
    public ActiveCache(TaskScheduler scheduler, Command<T> command)
    {
        this.scheduler = scheduler;
        this.command = command;
        this.refresher = new CacheRefresher();
    }

    /**
     * Constructs an active cached using the specified value loader and
     * expiration in seconds.
     * @param scheduler The task scheduler to use for scheduling tasks
     * @param command Value loader
     * @param expirySeconds Expiration in seconds
     */
    public ActiveCache(TaskScheduler scheduler, Command<T> command, int expirySeconds)
    {
        if (expirySeconds < 0)
            throw new IllegalArgumentException("expirySeconds must not be less than 0");

        this.scheduler = scheduler;
        this.expirySeconds = expirySeconds;
        this.command = command;
        this.refresher = new CacheRefresher();
    }

    /**
     * Starts the initial load and schedules future loads.
     */
    @Override
    public void start()
    {
        schedule(true);
    }

    /**
     * Cancels any existing scheduled tasks (if any).
     */
    private void cancel()
    {
        // cancel the existing thread (if any)
        if (future != null)
        {
            future.cancel(true);
            future = null;
        }
    }

    /**
     * Schedules the CacheRefresher with the thread executor service.  Cancels any
     * scheduled tasks before doing so.  If expirySeconds is 0, schedules only one
     * task.  If greater than 0, schedules a repeating task.
     * @param immediate If true, will schedule a task to load the cache immediately
     *                  If false, will delay the first load by expirySeconds
     */
    private void schedule(boolean immediate)
    {
        cancel();

        Calendar cal = Calendar.getInstance();
        if (! immediate)
            cal.add(Calendar.SECOND, expirySeconds);

        // schedule next load
        if (expirySeconds > 0)
            future = scheduler.scheduleAtFixedRate(refresher, cal.getTime(), expirySeconds * 1000);
        else
            future = scheduler.schedule(refresher, cal.getTime());
    }

    /**
     * Forces an explicit (unscheduled) reload of the cache.  Future cache refreshes
     * will be scheduled relative to this reload.
     */
    @Override
    public synchronized void expire()
    {
        schedule(true);
    }

    /**
     * Fetches the value from the cache.
     * @return Cached value
     */
    @Override
    public synchronized T get() throws CacheException
    {
        if (! initialized)
        {
            // if cache did not initialize properly, attempt to "lazily" reload it

            try
            {
                // if loadValue() is successful, invoking set() will delay the
                // next cache refresh by expirySeconds (or indefinitely if expirySeconds == 0)
                set(loadValue());
            }
            catch (Exception ex)
            {
                throw new CacheException("active cache is uninitialized; attempt to lazily reload failed", ex);
            }
        }

        // value is updated asynchronously, so no need to synchronize
        return value;
    }

    /**
     * Manually updates the value in the cache.  If the cache is expirable
     * (expirySeconds > 0) then it will cancel and reschedule the cache refresh.
     * @param value
     */
    @Override
    public synchronized void set(T value)
    {
        // cancel any running tasks
        cancel();

        // set the value
        this.value = value;
        initialized = true;

        // schedule a delayed task
        if (expirySeconds > 0)
            schedule(false);
    }

    /**
     * Closes down any running or scheduled tasks.
     */
    @Override
    public void close()
    {
        // cancels any running or scheduled tasks
        cancel();
    }

    /**
     * Retrieves a new value from the Command.
     * @throws CacheException
     */
    private T loadValue()
        throws Exception
    {
        T newValue = command.execute();

        if (command instanceof ValueMapper)
            newValue = ((ValueMapper<T>) command).mapValue(value, newValue);

        return newValue;
    }

    /**
     * Active caches will typically always have a value to return unless they
     * failed to initialize.
     * @return Returns true if the cache initialized properly; false otherwise
     */
    @Override
    public boolean hasValue()
    {
        return initialized;
    }

    /**
     * A Runnable wrapper to the value loader.  Invokes the value loader
     * and sets the value on the parent class.
     */
    private class CacheRefresher implements Runnable, Serializable
    {
        public void refresh()
            throws CacheException
        {
            synchronized (ActiveCache.this)
            {
                try
                {
                    LOG.debug("actively refreshing cache");

                    value = loadValue();

                    initialized = true;
                }
                catch (Exception ex)
                {
                    throw new CacheException("error actively loading value into cache", ex);
                }
            }
        }

        /**
         * Runnable callback which performs a cache refresh.  CacheExceptions will
         * be logged, but will not affect the schedule.
         */
        @Override
        public void run()
        {
            try
            {
                LOG.debug("actively refreshing cache");

                refresh();
            }
            catch (Exception ex)
            {
                LOG.debug("runnable caught", ex);
            }
        }
    }
}
