package com.mcmagi.triforce.cache;

import java.io.Serializable;

/**
 * A factory pattern for Caches.  Useful for managed caches which typically need
 * factory callbacks.  Defines two template variables: K - which is the type
 * that represents the data needed to initialize the cache (such as
 * configuration data or a cache "key"), and V - the type to be stored in the
 * cache.
 * @see CacheManager
 * @see CachedAttributeMap
 * @author Michael C. Maggio
 */
public interface CacheFactory<K,V> extends Serializable
{
    public Cache<V> createCache(K key);
}
