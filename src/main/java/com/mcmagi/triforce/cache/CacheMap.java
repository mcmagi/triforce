package com.mcmagi.triforce.cache;

import com.mcmagi.triforce.util.Startable;
import com.mcmagi.triforce.util.Closeable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author Michael C. Maggio
 */
public class CacheMap<K,V> implements ManagedCache, Closeable, Serializable
{
    private final ConcurrentMap<Key<K>,Cache<V>> cacheMap = new ConcurrentHashMap<>();
    private CacheFactory factory;

    public CacheMap(CacheFactory<K,V> factory)
    {
        this.factory = factory;
    }

    /**
     * Returns a value for the specified key.  If the value is not in the cache
     * it will be fetched.
     * @param key Cache key
     * @return Cache value
     * @throws CacheException 
     */
    public V get(K key)
        throws CacheException
    {
        Key<K> k = new Key(key);

        init(k);

        return cacheMap.get(k).get();
    }

    /**
     * Returns a value from the cache for the specified cache, but only if the
     * value is already cached.  Will return null if not cached.
     * @param key Cache key
     * @return Cache value
     * @throws CacheException 
     */
    public V getIfCached(K key)
        throws CacheException
    {
        Key<K> k = new Key(key);

        if (cacheMap.containsKey(k))
        {
            Cache<V> cache = cacheMap.get(k);
            synchronized (cache)
            {
                // TODO: not sure this is safe, might need to add getIfCached() on the Cache
                if (cache.hasValue())
                    return cache.get();
            }
        }
        return null;
    }

    /**
     * Returns a subset of cached entries in the map for which there is a loaded
     * value.
     * @param keys Keys in map to check
     * @return Cache map subset
     */
    public Map<K,V> getIfCached(Set<K> keys)
        throws CacheException
    {
        Map<K,V> valueMap = new HashMap<>();
        for (K key : keys)
        {
            V value = getIfCached(key);
            if (value != null)
                valueMap.put(key, value);
        }
        return valueMap;
    }

    /**
     * Puts a value in the cache.
     * @param key Cache key
     * @param value Cache value
     */
    public void put(K key, V value)
    {
        Key<K> k = new Key(key);

        init(k);

        cacheMap.get(k).set(value);
    }

    /**
     * Explicitly sets all elements of the map into the cache.
     * @param map Cache map
     */
    public void putAll(Map<K,V> map)
    {
        for (K key : map.keySet())
            put(key, map.get(key));
    }

    /**
     * Explicitly expires all elements in the cache.
     */
    @Override
    public void expire()
    {
        synchronized (cacheMap)
        {
            for (Key<K> key : cacheMap.keySet())
                cacheMap.get(key).expire();
        }
    }

    /**
     * Explicitly expire the specified element in the cache.
     * @param key Cache key
     */
    public void expire(K key)
    {
        Key k = new Key(key);

        if (cacheMap.containsKey(k))
            cacheMap.get(k).expire();
    }

    /**
     * Creates and initializes the cache for the specified key.  If the cache
     * is {@link Startable}, it will be started.
     * @param key
     */
    public void init(K key)
    {
        init(new Key(key));
    }

    private void init(Key<K> key)
    {
        Cache<V> newValue = factory.createCache(key.getKey());
        Cache<V> oldValue = cacheMap.putIfAbsent(key, factory.createCache(key.getKey()));
        if (oldValue == null)
        {
            // value was not in the map before; start it if needed
            if (newValue instanceof Startable)
                ((Startable) newValue).start();
        }
    }

    /**
     * Propagates close to any {@link Closeable} caches.
     */
    @Override
    public void close()
    {
        synchronized (cacheMap)
        {
            for (Cache<V> cache : cacheMap.values())
            {
                if (cache instanceof Closeable)
                    ((Closeable) cache).close();
            }
        }
    }

    /**
     * A wrapper around the key to ensure null keys are allowed.
     * @param <K> Key type
     */
    private static class Key<K> implements Serializable
    {
        private K m_key;

        public Key(K key)
        {
            this.m_key = key;
        }

        public K getKey() 
        {
            return m_key;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj == null || ! (obj instanceof Key))
                return false;

            Key k = (Key) obj;
            return new EqualsBuilder().append(m_key, k.m_key).isEquals();
        }

        @Override
        public int hashCode()
        {
            return new HashCodeBuilder().append(m_key).toHashCode();
        }
    }
}
