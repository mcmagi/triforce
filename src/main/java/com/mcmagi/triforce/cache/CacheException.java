package com.mcmagi.triforce.cache;

/**
 * An exception thrown by the caching implementation if there was a problem
 * accessing the cache or loading a value.
 * @author voyager
 */
public class CacheException extends Exception
{
    public CacheException(String message)
    {
        super(message);
    }

    public CacheException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
