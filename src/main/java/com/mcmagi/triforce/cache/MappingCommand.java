package com.mcmagi.triforce.cache;

import com.mcmagi.triforce.util.Command;

/**
 * A value loading Command which also maps the old value to the new.
 * @author Michael C. Maggio
 */
public interface MappingCommand<T> extends Command<T>, ValueMapper<T>
{
}
