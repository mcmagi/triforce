package com.mcmagi.triforce.cache;

import java.io.Serializable;

/**
 * Interface used for caches prior to Cache.  Provided for backward
 * compatability.  Please use Cache going forward.
 * @author Michael C. Maggio
 */
public interface CachedAttribute<T> extends Cache<T>
{
}
