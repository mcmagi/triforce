package com.mcmagi.triforce.cache;

/**
 * @author Michael C. Maggio
 */
public interface ManagedCache
{
    /**
     * Managed caches must be expirable.
     */
    public void expire();
}
