package com.mcmagi.triforce.cache;

/**
 * A simple expirable cache.
 * @author Michael C. Maggio
 */
public class SimpleCache<T> implements Cache<T>
{
    private int expirySeconds = 0;
    private long loadTime;
    private T value = null;

    /**
     * Constructs a simple cache with no expiration.
     */
    public SimpleCache()
    {
    }

    /**
     * Constructs a simple cache using the specified expiration in seconds.
     * @param expirySeconds Expiration in seconds
     */
    public SimpleCache(int expirySeconds)
    {
        if (expirySeconds < 0)
            throw new IllegalArgumentException("expirySeconds must not be less than 0");

        this.expirySeconds = expirySeconds;
    }

    /**
     * Fetches the value from the cache.  If the cache is expired, returns null.
     * @return Cached value
     */
    @Override
    public synchronized T get()
    {
        return isExpired() ? null : value;
    }

    /**
     * Manually updates the value in the cache.  Also updates the loadTime,
     * delaying expiration.
     * @param value
     */
    @Override
    public synchronized void set(T value)
    {
        loadTime = System.currentTimeMillis();
        this.value = value;
    }

    /**
     * Forces expiration of the cache.
     */
    @Override
    public synchronized void expire()
    {
        value = null;
    }

    /**
     * Returns whether the cache is expired.
     * @return True if expired
     */
    protected boolean isExpired()
    {
        // attribute was never loaded or forced expired
        if (value == null)
            return true;

        // no expiration - never expires
        if (expirySeconds == 0)
            return false;

        // compare expiration time to current time
        long expireTime = loadTime + (expirySeconds * 1000);
        long now = System.currentTimeMillis();

        return now >= expireTime;
    }

    /**
     * Returns true if the cache contains an unexpired value.
     * @return 
     */
    @Override
    public boolean hasValue()
    {
        return ! isExpired();
    }
}
