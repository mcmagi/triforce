package com.mcmagi.triforce.cache;

import java.io.Serializable;

/**
 * Represents a value is to be cached.  Implementations of
 * Cache may cache the value using various strategies.
 * Implementations must ensure they are serializable.
 * @author Michael C. Maggio
 */
public interface Cache<T> extends Serializable, ManagedCache
{
    /**
     * Fetches the value from the cache.  If the cache is
     * empty, loads the value as necessary.
     * @return Value in cache
     * @throws CacheException If error loading value
     */
    public T get() throws CacheException;

    /**
     * Manually updates the value in the cache.
     * @param value
     */
    public void set(T value);

    /**
     * Forces expiration of the cache.
     */
    @Override
    public void expire();

    /**
     * Returns true if there is a value stored in the cache.  Returns false if
     * a value must be fetched.
     * @return True if has value
     */
    public boolean hasValue();
}
