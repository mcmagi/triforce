package com.mcmagi.triforce.cache;

/**
 * @author Michael C. Maggio
 */
public interface ValueMapper<T>
{
    /**
     * Merges the old and new value, returning the mapped value.
     * @param oldValue Old value
     * @param newValue New value
     * @return Mapped value
     * @throws Exception
     */
    public T mapValue(T oldValue, T newValue) throws Exception;
}
