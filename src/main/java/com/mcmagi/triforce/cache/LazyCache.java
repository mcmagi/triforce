package com.mcmagi.triforce.cache;

import com.mcmagi.triforce.util.Command;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A lazily loading implementation of a Cache.  Values are retrieved
 * from the Command upon the first call to get(), then cached for a duration
 * equal to expirySeconds.  On the next call to get() after expirySeconds, the
 * value in the cache is refreshed.  If expirySeconds is 0, the cache never
 * expires.
 * @author Michael C. Maggio
 */
public class LazyCache<T> implements Cache<T>
{
    private static final Log log = LogFactory.getLog(LazyCache.class);

    private Command<T> command;
    private int expirySeconds = 0;
    private long loadTime;
    private T value;

    /**
     * Constructs a lazy cached using the specified command and no expiration.
     * @param command
     */
    public LazyCache(Command<T> command)
    {
        this.command = command;
    }

    /**
     * Constructs a lazy cache using the specified command and expiration
     * in seconds.
     * @param command
     * @param expirySeconds Expiration in seconds
     */
    public LazyCache(Command<T> command, int expirySeconds)
    {
        if (expirySeconds < 0)
            throw new IllegalArgumentException("expirySeconds must not be less than 0");

        this.expirySeconds = expirySeconds;
        this.command = command;
    }

    /**
     * Fetches the value from the cache.  If the cache is empty or expired,
     * then the cache is refreshed by calling the Command.
     * @return Cached value
     * @throws CacheException If error loading value
     */
    @Override
    public synchronized T get()
        throws CacheException
    {
        if (isExpired())
        {
            try
            {
                log.debug("lazily refreshing cache");

                T newValue = command.execute();

                if (command instanceof ValueMapper)
                    newValue = ((ValueMapper<T>) command).mapValue(value, newValue);

                set(newValue);
            }
            catch (Exception ex)
            {
                throw new CacheException("error lazily loading value into cache", ex);
            }
        }
        return value;
    }

    /**
     * Forces expiration of the cache.
     */
    @Override
    public synchronized void expire()
    {
        value = null;
    }

    /**
     * Returns whether the cache is expired.
     * @return True if expired
     */
    protected boolean isExpired()
    {
        // attribute was never loaded or force expired
        if (value == null)
            return true;

        // no expiration - never expires
        if (expirySeconds == 0)
            return false;

        // compare expiration time to current time
        long expireTime = loadTime + (expirySeconds * 1000);
        long now = System.currentTimeMillis();

        return now >= expireTime;
    }

    /**
     * Returns true if the cache contains an unexpired value.
     * @return 
     */
    @Override
    public boolean hasValue()
    {
        return ! isExpired();
    }

    /**
     * Manually updates the value in the cache.  Also updates the loadTime,
     * delaying expiration.
     * @param value
     */
    @Override
    public synchronized void set(T value)
    {
        this.value = value;
        loadTime = System.currentTimeMillis();
    }
}
