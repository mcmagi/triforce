
package com.mcmagi.triforce.cache;

import com.mcmagi.triforce.util.Command;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Michael C. Maggio
 */
public class MultiValueQueryCacheMap<K,V>
{
    private static final Log LOG = LogFactory.getLog(MultiValueQueryCacheMap.class);

    private Command<Map<K,V>> command;
    private CacheMap<K,V> cacheMap;

    public MultiValueQueryCacheMap(Command<Map<K,V>> command, CacheMap<K,V> cacheMap)
    {
		this.command = command;
        this.cacheMap = cacheMap;
    }

    /**
     * 
     * @param ids Object ids to load
     * @return Fetched objects
     * @throws CacheException
     */
    public Map<K,V> get(Set<K> ids)
        throws CacheException
    {
        LOG.debug("looking up ids from cache: " + ids);

        // get map of values from the cache that are not expired
        ids = new HashSet<>(ids);
        Map<K,V> valueMap = cacheMap.getIfCached(ids);
        Set<K> cachedIds = valueMap.keySet();
        LOG.debug("ids in cache: " + cachedIds);

        // get set of ids that are not in cache
        ids.removeAll(cachedIds);
        LOG.debug("ids not in cache: " + ids);

        // if we have item codes that need to be fetched, load them
        if (! ids.isEmpty())
            valueMap.putAll(load(ids));

        return valueMap;
    }

    /**
     * Loads the specified objects from the example service and stores
     * them in the cache.
     * @param ids Object ids to load
     * @return Loaded objects
     * @throws CacheException
     */
    private synchronized Map<K,V> load(Set<K> ids)
        throws CacheException
    {
        LOG.debug("preparing to fetch ids: " + ids);

        // need to re-check ids in case another thread did the update
        // while we were waiting for the synchronize lock; (this is basically
        // a double-checked lock)
        Map<K,V> resultMap = cacheMap.getIfCached(ids);
        Set<K> loadedIds = resultMap.keySet();
        LOG.debug("ids already loaded: " + loadedIds);

        if (! loadedIds.containsAll(ids))
        {
            // remove any ids thay may have been queried while we
            // were waiting for the lock
            ids.removeAll(loadedIds);

            // query for objects
			try
			{
            	LOG.debug("fetching unloaded ids: " + ids);
            	resultMap.putAll(command.execute(ids));
			}
			catch (Exception ex)
			{
				throw new CacheException("Failed to fetch values for ids: " + ids, ex);
			}

            // store in cache
            cacheMap.putAll(resultMap);
        }

        return resultMap;
    }
}
